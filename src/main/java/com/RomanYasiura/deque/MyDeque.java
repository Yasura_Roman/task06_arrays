package com.RomanYasiura.deque;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class MyDeque<T> implements Deque<T> {

    private ArrayList<T> elements;
    private int headIndex;
    private int tailIndex;

    public MyDeque() {
        this.elements = new ArrayList<T>();
        this.headIndex = 0;
        this.tailIndex = 0;
    }
    @Override
    public void addFirst(T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        elements.add(headIndex, t);
        tailIndex++;
    }

    @Override
    public void addLast(T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        this.elements.add(tailIndex, t);
        tailIndex++;
    }

    @Override
    public boolean offerFirst(T t) {
        addFirst(t);
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        addLast(t);
        return true;
    }

    @Override
    public T removeFirst() {
        T value = elements.remove(headIndex);
        this.tailIndex--;
        return value;
    }

    @Override
    public T removeLast() {
        T value = elements.remove(tailIndex - 1);
        this.tailIndex--;
        return value;
    }

    @Override
    public T pollFirst() {
        if (elements.isEmpty()) {
            return null;
        }
        return this.removeFirst();
    }

    @Override
    public T pollLast() {
        if (elements.isEmpty()) {
            return null;
        }
        return this.removeLast();
    }

    @Override
    public T getFirst() {
        return elements.get(headIndex);
    }

    @Override
    public T getLast() {
        return elements.get(tailIndex - 1);
    }

    @Override
    public T peekFirst() {
        if (elements.isEmpty()) {
            return null;
        }
        return getFirst();
    }

    @Override
    public T peekLast() {
        if (elements.isEmpty()) {
            return null;
        }
        return getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).equals(o)) {
                elements.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        for (int i = elements.size() - 1; i >= 0; i--) {
            if (elements.get(i).equals(o)) {
                elements.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(T t) {
        if (t == null) {
            return false;
        }
        this.elements.add(tailIndex, t);
        tailIndex++;
        return true;
    }

    @Override
    public boolean offer(T t) {
        return offerLast(t);
    }

    @Override
    public T remove() {
        return removeFirst();
    }

    @Override
    public T poll() {
        return pollFirst();
    }

    @Override
    public T element() {
        return getFirst();
    }

    @Override
    public T peek() {
        return peekFirst();
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        if (elements.remove(o)) {
            tailIndex--;
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public Iterator<T> iterator() {
        return elements.iterator();
    }

    @Override
    public Iterator<T> descendingIterator() {
        return elements.iterator();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return elements.toArray(a);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return elements.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return elements.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return elements.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return elements.retainAll(collection);
    }

    @Override
    public void clear() {
        this.elements.clear();
    }
}
