package com.RomanYasiura.logical;

import java.util.Arrays;

public class TaskArray {

    public int[] presenceInBoth(int[] firstArray, int[] secondArray){
        int[] result;
        int elementCount = 0;
        boolean addFlag;
        if(firstArray.length < secondArray.length){
            result = new int[firstArray.length];
        }else {
            result = new int[secondArray.length];
        }

        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {

                if (firstArray[i] == secondArray[j]){
                    addFlag = true;
                    for (int k = 0; k < elementCount; k++) {
                        if (firstArray[i] == result[k]){
                            addFlag = false;
                        }
                    }
                    if (addFlag){
                        result[elementCount] = firstArray[i];
                        elementCount++;
                    }
                }

            }
        }
        return Arrays.copyOf(result,elementCount);
    }

    public int[] presenceOnlyInOne(int[] firstArray, int[] secondArray){
        int[] result;
        int elementCount = 0;
        boolean addFlag;
        if(firstArray.length > secondArray.length){
            result = new int[firstArray.length];
        }else {
            result = new int[secondArray.length];
        }

        for (int i = 0; i < firstArray.length; i++) {
            addFlag = true;
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j]){
                    addFlag = false;
                }
            }
            if (addFlag){
                result[elementCount] = firstArray[i];
                elementCount++;
            }
        }
        return Arrays.copyOf(result,elementCount);
    }

    public int[] deleteIfPresentDuplicates(int[] array){
        boolean[] duplicate = new boolean[array.length];
        int[] result =  new int[array.length];
        int elementCount = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = ( i + 1 ); j < array.length; j++) {
                if(array[i] == array[j]){
                    duplicate[i] = true;
                    duplicate[j] = true;
                }
            }
        }

        for (int i = 0; i < duplicate.length ; i++) {
            if ( !duplicate[i]){
                result[elementCount] = array[i];
                elementCount++;
            }
        }
        return Arrays.copyOf(result,elementCount);
    }

    public int[] deleteIfPresentThreeOrMore(int[] array){
        int[] repeats =  new int[array.length];
        int[] result =  new int[array.length];
        int elementCount = 0;

        Arrays.fill(repeats,1);

        for (int i = 0; i < array.length; i++) {
            for (int j = ( i + 1 ); j < array.length; j++) {
                if(array[i] == array[j]){
                    repeats[i]++;
                    repeats[j]++;
                }
            }
        }

        System.out.println(Arrays.toString(repeats));
        for (int i = 0; i < array.length; i++) {
            if ( repeats[i] < 3 ){
                result[elementCount] = array[i];
                elementCount++;
            }
        }
        return Arrays.copyOf(result,elementCount);
    }

    public int[] removeRepeatableSeries(int[] array){
        int[] result =  new int[array.length];
        int elementCount = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] != array[ i - 1 ]){
                result[elementCount++] = array[ i - 1 ];
            }
        }
        result[elementCount++] = array[array.length - 1];

        return Arrays.copyOf(result,elementCount);
    }
}
