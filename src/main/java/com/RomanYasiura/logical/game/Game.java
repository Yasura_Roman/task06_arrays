package com.RomanYasiura.logical.game;

import java.util.*;

public class Game {

    private final int DOOR_COUNT = 10;
    private final int MIN_ARTIFACT_POWER = 10;
    private final int MAX_ARTIFACT_POWER = 80;
    private final int MIN_MONSTER_POWER = 5;
    private final int MAX_MONSTER_POWER = 100;
    private final int START_HERO_POWER = 25;
    private Random random = new Random();

    private int heroPower;
    private List<Door> doors;

    public Game(int heroPower) {
        this.heroPower = heroPower;
        createDoors();
    }

    private void createDoors(){
        int value;
        List<Door> list = new LinkedList<>();
        for (int i = 0; i < DOOR_COUNT ; i++) {
            if(random.nextInt(20) < 10 ){
                //MONSTER
                value = (-1) * (random.nextInt(MAX_MONSTER_POWER - MIN_MONSTER_POWER) + MIN_MONSTER_POWER);
            } else {
                //ARTIFACT
                value = random.nextInt(MAX_ARTIFACT_POWER-MIN_ARTIFACT_POWER) + MIN_ARTIFACT_POWER;
            }
            list.add(new Door(i, value,true));
        }
        doors = list;
    }

    public int deedDoorCount(){
        int deedDoor = 0;
        for (Door door: doors
             ) {
            if (door.isClose() == true){
                if (door.getPower() < 0 && heroPower < Math.abs(door.getPower())){
                    deedDoor++;
                }
            }
        }
        return deedDoor;
    }

    public int[] bestSequence(){
        int[] doorSequence = new int[DOOR_COUNT];
        int power = START_HERO_POWER;
        List<Door> sortedDoors = new LinkedList<>();
        sortedDoors.addAll(doors);
        sortedDoors.sort(Door::compareTo);
        for (int i = 0; i < sortedDoors.size(); i++) {
            doorSequence[i] = sortedDoors.get(i).getName();
        }
        return doorSequence;
    }

    public boolean itsWinningGame(){
        int power = START_HERO_POWER;
        List<Door> sortedDoors = new LinkedList<>();
        sortedDoors.addAll(doors);
        sortedDoors.sort(Door::compareTo);
        for (int i = sortedDoors.size() -1; i <= 0; i--) {
            if (sortedDoors.get(i).getPower() > 0){
                power += sortedDoors.get(i).getPower();
            }else if (sortedDoors.get(i).getPower() < 0 ){
                if (power < Math.abs(sortedDoors.get(i).getPower())){
                    return false;
                }
            }
        }
        return true;
    }

    public List<Door> getDoors() {
        return doors;
    }
}
