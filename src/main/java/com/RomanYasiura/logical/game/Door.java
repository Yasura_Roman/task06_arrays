package com.RomanYasiura.logical.game;

public class Door implements Comparable<Door>{
    private int name;
    private int power;
    private boolean close;

    public Door(int name, int power, boolean close) {
        this.name = name;
        this.power = power;
        this.close = close;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    @Override
    public int compareTo(Door door) {
        if (this.getPower() > door.getPower()) {
            return 1;
        } else if (this.getPower() < door.getPower()){
            return -1;
        }
        return 0;
    }
}
