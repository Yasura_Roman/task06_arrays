package com.RomanYasiura.pq;

import java.util.*;

public class MyPriorityQueue<T extends Comparable<T>> extends AbstractQueue<T>{

    private ArrayList<T> elements;
    private Comparator<T> comparator;


    public MyPriorityQueue(Comparator<T> comparator){
        elements = new ArrayList<>();
        this.comparator = comparator;
    }

    public MyPriorityQueue(){
        this(Comparator.naturalOrder());
    }

    @Override
    public Iterator<T> iterator() {
        return elements.iterator();
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean offer(T t) {
        if (elements.add(t)) {
            Collections.sort(elements, comparator);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public T poll() {
        if (elements.isEmpty()) {
            return null;
        }
        return elements.remove(0);
    }

    @Override
    public T peek() {
        return elements.get(0);
    }

    @Override
    public boolean add(T element) {
        return offer(element);
    }

    @Override
    public T remove() {
        return elements.remove(0);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public boolean contains(Object obj) {
        return elements.contains(obj);
    }
}
