package com.RomanYasiura.task3;

import java.util.Comparator;

public class TwoStringComparator implements Comparator<TwoStringContainer> {
    @Override
    public int compare(TwoStringContainer s1, TwoStringContainer s2) {
        return s1.getSecond().compareTo(s2.getSecond());
    }
}
