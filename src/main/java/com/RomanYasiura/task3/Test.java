package com.RomanYasiura.task3;

import com.RomanYasiura.mvc.view.CountryCapitalGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Test {

    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        CountryCapitalGenerator generator = new CountryCapitalGenerator();
        List<TwoStringContainer>  list = generator.generate(10);
        Collections.sort(list);
        logger.info("Country compare:");
        logger.info(Arrays.toString(list.toArray()));
        Collections.sort(list,new TwoStringComparator());
        logger.info("Capital compare:");
        logger.info(Arrays.toString(list.toArray()));
    }
}
