package com.RomanYasiura.task3;

public class TwoStringContainer implements Comparable<TwoStringContainer> {

    private String first;
    private String second;

    public TwoStringContainer() {
    }

    public TwoStringContainer(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int compareTo(TwoStringContainer other) {
        return first.compareTo(other.first);
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "[first = " + first + ", second = " + second + "]";
    }
}
