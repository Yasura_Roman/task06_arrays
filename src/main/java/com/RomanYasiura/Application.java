package com.RomanYasiura;

import com.RomanYasiura.mvc.view.GameView;

public class Application {
    public static void main(String[] args) {
        GameView view = new GameView();
        view.show();
    }
}
