package com.RomanYasiura.mvc.controller;

public interface GameController {
        void createGame();
        String[] infoTable();
        int deedDoorsCount();
        String winningSequence();
}
