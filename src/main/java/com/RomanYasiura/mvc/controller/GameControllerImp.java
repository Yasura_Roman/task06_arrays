package com.RomanYasiura.mvc.controller;

import com.RomanYasiura.mvc.model.GameModel;
import com.RomanYasiura.mvc.model.GameModelImp;

public class GameControllerImp implements GameController {

    private GameModel model;

    public GameControllerImp() {
        model = new GameModelImp();
    }

    @Override
    public void createGame() {
        model.createGame();
    }

    @Override
    public String[] infoTable() {
        return model.infoTable();
    }

    @Override
    public int deedDoorsCount() {
        return model.deedDoorsCount();
    }

    @Override
    public String winningSequence() {
        return model.winningSequence();
    }
}
