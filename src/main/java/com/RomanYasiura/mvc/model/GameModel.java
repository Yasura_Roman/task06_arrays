package com.RomanYasiura.mvc.model;

public interface GameModel {
    void createGame();
    String[] infoTable();
    int deedDoorsCount();
    String winningSequence();
}
