package com.RomanYasiura.mvc.model;

import com.RomanYasiura.logical.game.Door;
import com.RomanYasiura.logical.game.Game;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GameModelImp implements GameModel {

    private Game game;

    public GameModelImp(){
        game = new Game(25);
    }

    @Override
    public void createGame() {
        game = new Game(25);
    }

    @Override
    public String[] infoTable() {
        List<Door> list = game.getDoors();
        String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i).getName()+ "\t";
            if (list.get(i).isClose()){
                array[i] += "close";
            }else {
            array[i] += "open";
            }
            array[i] += "\t" + list.get(i).getPower();
        }
        return array;
    }

    @Override
    public int deedDoorsCount() {
        return game.deedDoorCount();
    }

    @Override
    public String winningSequence() {
        if (game.itsWinningGame()){
            return "winning sequence : "+ Arrays.toString( game.bestSequence());
        }
        return "does not exist winning sequence";
    }
}
