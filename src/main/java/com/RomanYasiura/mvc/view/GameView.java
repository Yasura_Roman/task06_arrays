package com.RomanYasiura.mvc.view;

import com.RomanYasiura.mvc.controller.GameController;
import com.RomanYasiura.mvc.controller.GameControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class GameView {
    private GameController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public GameView() {
        controller = new GameControllerImp() {
        };
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - create Game");
        menu.put("2", "  2 - show Doors");
        menu.put("3", "  3 - deed doors count");
        menu.put("4", "  4 - winning sequence");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1(){
        controller.createGame();
        logger.info("Create game");
    }

    private void pressButton2() {
        String[]table = controller.infoTable();
        logger.info("\tТаблиця");
        logger.info("\t№\tisOpen\tpowerUp");
        logger.info(" -----------------------");
        for (int i = 0; i < table.length; i++) {
            logger.info("|\t" + table[i] + "\t|");
        }
        logger.info(" -----------------------");
    }

    private void pressButton3() {

        int count = controller.deedDoorsCount();
        logger.info("deed doors count = " + count + ";");
    }

    private void pressButton4() {
        String sequence = controller.winningSequence();
        logger.info("winning sequence " + sequence + ";");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
