package com.RomanYasiura.mvc.view;

public interface Printable {
    void print();
}
