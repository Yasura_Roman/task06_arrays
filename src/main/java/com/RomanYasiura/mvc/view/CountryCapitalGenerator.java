package com.RomanYasiura.mvc.view;

import com.RomanYasiura.task3.TwoStringContainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CountryCapitalGenerator {

    Random random = new Random();
    String[] country = {"ARGENTINA","ARMENIA","LAOS","ECUADOR","CUBA","PERU","BULGARIA","HUNGARY","MALI","BELIZE"};
    String[] capital = {"BUENOS AIRES","YEREVAN","VIENTIANE","QUITO","HAVANA","LIMA","SOFIA","BUDAPEST","BAMAKO","BELMOPAN"};

    public List<TwoStringContainer> generate(int count){
        List<TwoStringContainer> list = new ArrayList<>();
        int value;
        for (int i = 0; i < count ; i++) {
            value = random.nextInt(100)%10;
            list.add(new TwoStringContainer(country[value],capital[value]));
        }
        return list;
    }

}
